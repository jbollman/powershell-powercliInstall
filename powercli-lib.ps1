
function spawn-clones {
    <#
    .SYNOPSIS
        Generates multiple vms in vcenter
    DESCRTIPTION
        simple command to create multipe vms. Good for mass testing. 
    .EXAMPLE
        spawn-clones -numberOfVMS 2 -spawnName 'zzzVMS'
    .EXAMPLE
        spawn-clones -templateVM 'RHEL70template'
    
    #>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true)][int]$numberOfVMS = 3,
    [Parameter(Mandatory=$true)][string]$spawnName = 'TESTVM',
    [Parameter(Mandatory=$false)][string]$spawnPath,
    [Parameter(Mandatory=$false)][string]$templateVM = 'Centos 7 64-bit-6.5 compatible'
)
# if path is supplied, AKA a file of vms already exist, then the if will run
    if ($spawnPath) {
        $spawnVmNames = gc $spawnPath
        foreach ($vm in $spawnVmNames) {
            New-VM -Name $vm -VM $templateVM -VMHost (Get-VMHost | get-random)
            sleep 2
       }
       # else runs a standard TESTVM 1,2,3 standard
    } else {
        $globalArray = @();
        for ($i=0; $i -lt $numberOfVMS; $i++){
            New-VM -Name "$spawnName $i" -VM $templateVM -VMHost (Get-VMHost | get-random)
            $globalArray += "$spawnName $i";
            sleep 2
        }
        echo "global array variable contains: $globalArray"
        $globalArray | Out-File ~/spawnVmNames.txt
    }
    
}



function spawn-powerON {
    <#
    .SYNOPSIS
        Power on multiple  vms in vcenter
    DESCRTIPTION
        simple command to power on  multipe vms.
    .EXAMPLE
        spawn-powerON -fileLocation C:\Users\joseph.bollman\spawnVmNames.txt
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)][string]$spawnPath
    )
     $spawnVmNames = gc $fileLocation

     foreach ($vm in $spawnVmNames) {
        Start-VM -VM $vm -Confirm:$false -ErrorAction Continue
     }
}


function destroy-clones {
    <#
    .SYNOPSIS
        Power on multiple  vms in vcenter
    DESCRTIPTION
        Cleans up all vms that were spawned earlier. Can be any list of vms that is passed into the command.
    .EXAMPLE
        destroy-clones -fileLocation C:\Users\joseph.bollman\spawnVmNames.txt
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)][string]$spawnPath
    )
    $spawnVmNames = gc $fileLocation

    foreach ($vm in $spawnVmNames) {
        if ((Get-VM -Name $vm).PowerState -eq 'PoweredOn') {
            Stop-VM -Kill -VM $vm -Confirm:$false -Verbose
            sleep 2
        }
        if (Get-VM -Name $vm) {
            Remove-vm -vm $vm -Confirm:$false -DeletePermanently -Verbose
            sleep 3
        }
    }
}


function mass-move {
    <#
    .SYNOPSIS
        moves multiple vms to a new datastore
    DESCRTIPTION
        Moves a large amount of vms to a new datastore. You are responsible to determine if DS is large enough to handle the new vms.
    .EXAMPLE
        mass-move -fileLocation C:\Users\joseph.bollman\spawnVmNames.txt -dataStore ds50
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)][string]$spawnPath,
        [Parameter(Mandatory=$true)][string]$dataStore

    )
    $VmNames = gc $fileLocation
    
    foreach ($vm in $VmNames) {
        Move-VM $vm -Datastore $dataStore -RunAsync -ErrorAction continue
    }

}
