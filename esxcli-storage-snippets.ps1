esxcli storage filesystem list
## this will list all vmfs/nfs datastores connected to a host


esxcli storage vmfs snapshot list
## this will list vmfs snapshots that have never been mounted?  once its been mounted once, the uuid will not be listed here.


#mount vmfs filesystem
esxcli storage filesystem mount --volume-uuid 5cc0cfe1-588021e8-404b-005056b986ca

#unmount vmfs filesystem
esxcli storage filesystem unmount --volume-uuid 5cc0cfe1-588021e8-404b-005056b986ca

#mount vmfs snapshot resignature?
esxcli storage vmfs snapshot resignature --volume-uuid 5caf9147-xxxxc803-5186-xxxxxxxxxxxx

